let squareColor = 200;
let img;

function preload() {
  img = loadImage('junior.jpg');
}

function setup() {
  createCanvas(400, 400);
}

function draw() {

  if (mouseX < width / 2) {
    background(255, 0, 0);
  }

  else {
    background(0, 255, 0);
  }

  if (mouseX > 100 && mouseX < 300 && mouseY > 100 && mouseY < 300) {
    image(img, 100, 100, 200, 200);
  } else {
    squareColor = 200;
    fill(squareColor);
    rect(100, 100, 200, 200);
  }
}

